import os
import subprocess

os.environ.update({'TEXINPUTS': '.:..:../..:'})

annees = [_ for _ in os.listdir('.') if len(_) == 4 and _[:2] == '20']
for annee in annees:
    os.chdir(annee)
    lieux = [_ for _ in os.listdir('.')]
    for lieu in lieux:
        os.chdir(lieu)
        docs = [_ for _ in os.listdir('.') if 'tex' in _]
        for doc in docs:
            sortie = subprocess.run(['pdflatex', doc])
            sortie = subprocess.run(['pdflatex', doc])
        os.chdir('..')
    os.chdir('..')
